<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

// t3lib_extMgm
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'Fti.' . $_EXTKEY,
    'Newsserver',
    'Crisis News Feed'
);

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'Fti.' . $_EXTKEY,
	'SinglePage',
	'Crisis News Single Page Display'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Crisis Feed Extension');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_crisisfeed_domain_model_news');

$extPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY);
$extRelPath = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY);

$TCA['tx_crisisfeed_domain_model_news'] = array (
	'ctrl' => array (
		'title'             => 'LLL:EXT:crisisfeed/Resources/Private/Language/locallang_db.xlf:tx_crisisfeed_domain_model_news',
		'label'				=> 'title',
		'tstamp'            => 'tstamp',
		'crdate'            => 'crdate',
		'delete'            => 'deleted',
		'enablecolumns'     => array(
		 	'disabled' => 'hidden'
		),
		'dynamicConfigFile' => $extPath . 'Configuration/TCA/tx_crisisfeed_domain_model_news.php',
		'iconfile'          => $extRelPath . 'Resources/Public/Icons/crisisfeed_domain_model_news.gif'
	)
);

$TCA['tx_crisisfeed_domain_model_currentnews'] = array (
	'ctrl' => array (
		'title'             => 'LLL:EXT:crisisfeed/Resources/Private/Language/locallang_db.xlf:tx_crisisfeed_domain_model_currentnews',
		'label'				=> 'title',
		'enablecolumns'     => array(
			'disabled' => 'hidden'
		),
		'dynamicConfigFile' => $extPath . 'Configuration/TCA/tx_crisisfeed_domain_model_currentnews.php',
		'iconfile'          => $extRelPath . 'Resources/Public/Icons/crisisfeed_domain_model_news.gif'
	)
);

$TCA['tx_crisisfeed_domain_model_overlaynews'] = array (
    'ctrl' => array (
        'title'             => 'LLL:EXT:crisisfeed/Resources/Private/Language/locallang_db.xlf:tx_crisisfeed_domain_model_overlaynews',
        'label'				=> 'title',
        'enablecolumns'     => array(
            'disabled' => 'hidden'
        ),
        'dynamicConfigFile' => $extPath . 'Configuration/TCA/tx_crisisfeed_domain_model_overlaynews.php',
        'iconfile'          => $extRelPath . 'Resources/Public/Icons/crisisfeed_domain_model_news.gif'
    )
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
	'tx_crisisfeed_domain_model_news', $extPath . 'Resources/Private/Language/locallang_csh_news.xml'
);

?>