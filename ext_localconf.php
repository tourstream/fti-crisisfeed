<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

// Tx_Extbase_Utility_Extension
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Fti.' . $_EXTKEY,
    'Newsserver',
	array(
		'Index' => 'getActiveRssFeed, error',
	),
	array(
		'Index' => 'getActiveRssFeed, error',
	)
);

// The Plugin that renders the single page
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'Fti.' . $_EXTKEY,
	'SinglePage',
	array(
		'Display' => 'index',
	),
	array(
	)
);

// The Plugin that renders the overlay
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Fti.' . $_EXTKEY,
    'Newsoverlay',
    array(
        'Overlay' => 'index',
    ),
    array(
    )
);

// Register scheduler tasks
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['Fti\\Crisisfeed\\Task\\Importer'] = array(
	'extension' => $_EXTKEY,
	'title' => 'Crisis News Feed Importer',
	'description' => 'Imports current crisis news feed',
	'additionalFields' => '',
);

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks']['Fti\\Crisisfeed\\Task\\Cleanup'] = array(
	'extension' => $_EXTKEY,
	'title' => 'Crisis News Cleanup',
	'description' => 'Performs some cleanup tasks on the News server',
	'additionalFields' => '',
);

// Register Hook for Saving News record
if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'])) {
	$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tceforms.php']['getSingleFieldClass'] = array();
}

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass'][] =
	'\\Fti\\Crisisfeed\\Hook\\NewsSave';
