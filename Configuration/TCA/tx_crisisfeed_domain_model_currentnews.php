<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

$ll = 'LLL:EXT:crisisfeed/Resources/Private/Language/locallang_db.xlf:';

$TCA['tx_crisisfeed_domain_model_currentnews'] = array(
	'ctrl' => $TCA['tx_crisisfeed_domain_model_currentnews']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, header, subheader'
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title, header, subheader, bodytext, datetime, author, image, portal_test, portal_fti_de, portal_fti_at, portal_fti_ch, portal_5vorflug_de, portal_sonnenklar_tv, portal_bigextra_de, portal_lal_de, portal_fti_cruises_com, --div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access,starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => '')
	),
	'columns' => array(
		'pid' => array(
			'label' => 'pid',
			'config' => array(
				'type' => 'passthrough'
			)
		),
		'title' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.title',
			'config' => array(
				'type' => 'input',
				'size' => 100,
				'eval' => 'required',
			)
		),
		'header' => array(
			'exclude' => 1,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.header',
			'config' => array(
				'type' => 'input',
				'size' => 150,
				'eval' => 'required',
			)
		),
		'subheader' => array(
			'exclude' => 1,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.subheader',
			'config' => array(
				'type' => 'text',
				'cols' => 250,
				'rows' => 5,
			)
		),
		'bodytext' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.bodytext',
			'config' => array(
				'type' => 'text',
				'cols' => 30,
				'rows' => 5,
				'eval' => 'required',
				'softref' => 'rtehtmlarea_images,typolink_tag,images,email[subst],url',
				'wizards' => array(
					'_PADDING' => 2,
					'RTE' => array(
						'notNewRecords' => 1,
						'RTEonly' => 1,
						'type' => 'script',
						'title' => 'Full screen Rich Text Editing',
						'icon' => 'wizard_rte2.gif',
						'module' => array(
							'name' => 'wizard_rte',
						),
					),
				),
			)
		),
		'datetime' => array(
			'exclude' => 0,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => $ll . 'tx_crisisfeed_domain_model_news.datetime',
			'config' => array(
				'type' => 'input',
				'size' => 12,
				'max' => 20,
				'eval' => 'datetime,required',
				'default' => mktime(date('H'), date('i'), 0, date('m'), date('d'), date('Y'))
			)
		),
		'author' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.author',
			'config' => array(
				'type' => 'input',
				'size' => 100,
			)
		),
		'image' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.image',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'file',
				'allowed' => 'gif,png,jpeg,jpg',
				'max_size' => 100,
				// 'uploadfolder' => 'uploads/tx_crisisfeed',
				'show_thumbs' => 1,
				'size' => 1,
				'minitems' => 0,
				'maxitems' => 1,
			)
		),
		'guid' => array(
			'label' => 'guid',
			'config' => array(
				'type' => 'passthrough',
			)
		),
	),
);