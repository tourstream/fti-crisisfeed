<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

$ll = 'LLL:EXT:crisisfeed/Resources/Private/Language/locallang_db.xlf:';

$TCA['tx_crisisfeed_domain_model_overlaynews'] = array(
	'ctrl' => $TCA['tx_crisisfeed_domain_model_overlaynews']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, live, hidden, title, header, subheader'
	),
	'types' => array(
		//'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, live, hidden;;1, title, header, subheader, bodytext, --div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access,starttime, endtime'),
			'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, live, hidden;;1, title, header, subheader, bodytext;;;richtext::rte_transform[flag=rte_disabled|mode=ts_css], datetime, lastupdated, portal_test, portal_fti_de, portal_fti_at, portal_fti_ch, portal_5vorflug_de, portal_sonnenklar_tv, portal_bigextra_de, portal_lal_de, portal_fti_cruises_com, --div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access,starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => '')
	),
	'columns' => array(
		'pid' => array(
			'label' => 'pid',
			'config' => array(
				'type' => 'passthrough'
			)
		),
        'live' => array(
            'exclude' => 1,
            'label' => $ll . 'tx_crisisfeed_domain_model_overlaynews.live',
            'config' => array(
                'type' => 'check',
                'default' => 0
            )
        ),
        'hidden' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
            'config' => array(
                'type' => 'check',
                'default' => 0
            )
        ),
		'title' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_crisisfeed_domain_model_overlaynews.title',
			'config' => array(
				'type' => 'input',
				'size' => 100,
				'eval' => 'required',
			)
		),
		'header' => array(
			'exclude' => 1,
			'label' => $ll . 'tx_crisisfeed_domain_model_overlaynews.header',
			'config' => array(
				'type' => 'input',
				'size' => 150,
			)
		),
		'subheader' => array(
			'exclude' => 1,
			'label' => $ll . 'tx_crisisfeed_domain_model_overlaynews.subheader',
			'config' => array(
				'type' => 'input',
				'size' => 150,
			)
		),
		'bodytext' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_crisisfeed_domain_model_overlaynews.bodytext',
			'config' => array(
				'type' => 'text',
				'cols' => 90,
				'rows' => 10,
				'eval' => 'required',
				'softref' => 'rtehtmlarea_images,typolink_tag,images,email[subst],url',
				'wizards' => array(
					'_PADDING' => 2,
					'RTE' => array(
						'notNewRecords' => 1,
						'RTEonly' => 1,
						'type' => 'script',
						'title' => 'Full screen Rich Text Editing',
						'icon' => 'wizard_rte2.gif',
						'module' => array(
							'name' => 'wizard_rte',
						),
					),
				),
			),
            'defaultExtras' => 'richtext[]',
		),
		'guid' => array(
			'label' => 'guid',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'portal_test' => array(
				'exclude' => 0,
				'label' => $ll . 'tx_crisisfeed_domain_model_news.portal_test',
				'config' => array(
						'type' => 'check',
				)
		),
		'portal_fti_de' => array(
				'exclude' => 0,
				'label' => $ll . 'tx_crisisfeed_domain_model_news.portal_fti_de',
				'config' => array(
						'type' => 'check',
				)
		),
		'portal_fti_ch' => array(
				'exclude' => 0,
				'label' => $ll . 'tx_crisisfeed_domain_model_news.portal_fti_ch',
				'config' => array(
						'type' => 'check',
				)
		),
		'portal_fti_at' => array(
				'exclude' => 0,
				'label' => $ll . 'tx_crisisfeed_domain_model_news.portal_fti_at',
				'config' => array(
						'type' => 'check',
				)
		),
		'portal_5vorflug_de' => array(
				'exclude' => 0,
				'label' => $ll . 'tx_crisisfeed_domain_model_news.portal_5vorflug_de',
				'config' => array(
						'type' => 'check',
				)
		),
		'portal_sonnenklar_tv' => array(
				'exclude' => 0,
				'label' => $ll . 'tx_crisisfeed_domain_model_news.portal_sonnenklar_tv',
				'config' => array(
						'type' => 'check',
				)
		),
		'portal_bigextra_de' => array(
				'exclude' => 0,
				'label' => $ll . 'tx_crisisfeed_domain_model_news.portal_bigextra_de',
				'config' => array(
						'type' => 'check',
				)
		),
		'portal_lal_de' => array(
				'exclude' => 0,
				'label' => $ll . 'tx_crisisfeed_domain_model_news.portal_lal_de',
				'config' => array(
						'type' => 'check',
				)
		),
		'portal_fti_cruises_com' => array(
				'exclude' => 0,
				'label' => $ll . 'tx_crisisfeed_domain_model_news.portal_fti_cruises_com',
				'config' => array(
						'type' => 'check',
				)
		),
        'starttime' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:cms/locallang_ttc.xml:starttime_formlabel',
            'config' => array(
                'type' => 'input',
                'size' => 8,
                'max' => 20,
                'eval' => 'datetime',
                'default' => 0,
            )
        ),
        'endtime' => array(
            'exclude' => 1,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:cms/locallang_ttc.xml:endtime_formlabel',
            'config' => array(
                'type' => 'input',
                'size' => 8,
                'max' => 20,
                'eval' => 'datetime',
                'default' => 0,
            )
        ),
	),
);