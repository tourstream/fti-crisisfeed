<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

$ll = 'LLL:EXT:crisisfeed/Resources/Private/Language/locallang_db.xlf:';

$TCA['tx_crisisfeed_domain_model_news'] = array(
	'ctrl' => $TCA['tx_crisisfeed_domain_model_news']['ctrl'],
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, header, subheader, lastupdated'
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title, header, subheader, bodytext;;;richtext::rte_transform[flag=rte_disabled|mode=ts_css], datetime, lastupdated, author, image, portal_test, portal_fti_de, portal_fti_at, portal_fti_ch, portal_5vorflug_de, portal_sonnenklar_tv, portal_bigextra_de, portal_lal_de, portal_fti_cruises_com, --div--;LLL:EXT:cms/locallang_ttc.xml:tabs.access,starttime, endtime'),
	),
	'palettes' => array(
		'1' => array('showitem' => '')
	),
	'columns' => array(
		'pid' => array(
			'label' => 'pid',
			'config' => array(
				'type' => 'passthrough'
			)
		),
		'sys_language_uid' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:cms/locallang_ttc.xml:sys_language_uid_formlabel',
			'config' => array(
				'type' => 'select',
				'foreign_table' => 'sys_language',
				'foreign_table_where' => 'ORDER BY sys_language.title',
				'items' => array(
					array('LLL:EXT:lang/locallang_general.xml:LGL.allLanguages', -1),
					array('LLL:EXT:lang/locallang_general.xml:LGL.default_value', 0)
				)
			)
		),
		'l10n_parent' => array(
			'config' => array(
				'type' => 'passthrough'
			)
		),
		'l10n_diffsource' => array(
			'config' => array(
				'type' => 'passthrough'
			)
		),
		'hidden' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:lang/locallang_general.xml:LGL.hidden',
			'config' => array(
				'type' => 'check',
				'default' => 0
			)
		),
		'tstamp' => array(
			'label' => 'tstamp',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'crdate' => array(
			'label' => 'crdate',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'cruser_id' => array(
			'label' => 'cruser_id',
			'config' => array(
				'type' => 'passthrough'
			)
		),
		'starttime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:cms/locallang_ttc.xml:starttime_formlabel',
			'config' => array(
				'type' => 'input',
				'size' => 8,
				'max' => 20,
				'eval' => 'datetime',
				'default' => 0,
			)
		),
		'endtime' => array(
			'exclude' => 1,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => 'LLL:EXT:cms/locallang_ttc.xml:endtime_formlabel',
			'config' => array(
				'type' => 'input',
				'size' => 8,
				'max' => 20,
				'eval' => 'datetime',
				'default' => 0,
			)
		),
		'title' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.title',
			'config' => array(
				'type' => 'input',
				'size' => 100,
				'eval' => 'required',
			)
		),
		'header' => array(
			'exclude' => 1,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.header',
			'config' => array(
				'type' => 'input',
				'size' => 150,
				'eval' => 'required',
			)
		),
		'subheader' => array(
			'exclude' => 1,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.subheader',
			'config' => array(
				'type' => 'text',
				'cols' => 250,
				'rows' => 5,
			)
		),
		'bodytext' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.bodytext',
			'defaultExtras' => 'richtext[cut|copy|paste|bold|italic|underline|orderedlist|unorderedlist|link|line|chMode]',
			'config' => array(
				'type' => 'text',
				'cols' => 30,
				'rows' => 5,
				'eval' => 'required',
				'softref' => 'rtehtmlarea_images,typolink_tag,images,email[subst],url',
				'wizards' => array(
					'_PADDING' => 2,
					'RTE' => array(
						'notNewRecords' => 1,
						'RTEonly' => 1,
						'type' => 'script',
						'title' => 'Full screen Rich Text Editing',
						'icon' => 'wizard_rte2.gif',
						'module' => array(
							'name' => 'wizard_rte',
						),
					),
				),
			)
		),
		'datetime' => array(
			'exclude' => 0,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => $ll . 'tx_crisisfeed_domain_model_news.datetime',
			'config' => array(
				'type' => 'input',
				'size' => 12,
				'max' => 20,
				'eval' => 'datetime,required',
				'readOnly' =>1,
				'default' => mktime(date('H'), date('i'), 0, date('m'), date('d'), date('Y'))
			)
		),
		'lastupdated' => array(
			'exclude' => 0,
			'l10n_mode' => 'mergeIfNotBlank',
			'label' => $ll . 'tx_crisisfeed_domain_model_news.lastupdated',
			'config' => array(
				'type' => 'input',
				'size' => 12,
				'max' => 20,
				'eval' => 'datetime,required',
				'readOnly' => 1,
				'default' => mktime(date('H'), date('i'), 0, date('m'), date('d'), date('Y'))
			)
		),
		'author' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.author',
			'config' => array(
				'type' => 'input',
				'size' => 100,
			)
		),
		'image' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.image',
			'config' => array(
				'type' => 'group',
				'internal_type' => 'file',
				'allowed' => 'gif,png,jpeg,jpg',
				'max_size' => 100,
				'show_thumbs' => 1,
				'size' => 1,
				'minitems' => 0,
				'maxitems' => 1,
			)
		),
		'guid' => array(
			'label' => 'guid',
			'config' => array(
				'type' => 'passthrough',
			)
		),
		'portal_test' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.portal_test',
			'config' => array(
				'type' => 'check',
			)
		),
		'portal_fti_de' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.portal_fti_de',
			'config' => array(
				'type' => 'check',
			)
		),
		'portal_fti_ch' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.portal_fti_ch',
			'config' => array(
				'type' => 'check',
			)
		),
		'portal_fti_at' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.portal_fti_at',
			'config' => array(
				'type' => 'check',
			)
		),
		'portal_5vorflug_de' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.portal_5vorflug_de',
			'config' => array(
				'type' => 'check',
			)
		),
		'portal_sonnenklar_tv' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.portal_sonnenklar_tv',
			'config' => array(
				'type' => 'check',
			)
		),
		'portal_bigextra_de' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.portal_bigextra_de',
			'config' => array(
				'type' => 'check',
			)
		),
		'portal_lal_de' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.portal_lal_de',
			'config' => array(
				'type' => 'check',
			)
		),
		'portal_fti_cruises_com' => array(
			'exclude' => 0,
			'label' => $ll . 'tx_crisisfeed_domain_model_news.portal_fti_cruises_com',
			'config' => array(
				'type' => 'check',
			)
		),
	),
);