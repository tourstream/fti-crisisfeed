module.exports = function (grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            options: {
                banner: '/*! <%= grunt.template.today("yyyy-mm-dd hh:mm:ss") %> */'
            },
            js: {
                files: {
                    'Resources/Public/Javascript/overlay.min.js': ['Resources/Public/Javascript/overlay.js']
                }
            }
        },

        watch: {
            js: {
                files: ['Gruntfile.js', 'Resources/Public/Javascript/overlay.js'],
                tasks: ['jshint:beforeconcat', 'concat:js', 'uglify']
            }
        }
    });


    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task.
    grunt.registerTask(
        'default',
        'Build process for developing with Sass',
        [
            'uglify',
            'watch'
        ]
    );

    grunt.registerTask(
        'build',
        'Start a build process to build all stuff for deploy or commit.',
        [
            'uglify'
        ]
    );
};
