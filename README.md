﻿# crisisfeed
## purpose
- Show crisisfeed overlay
- Tasks for import and cleanup crisis news

## used by
- on all pages to display urgent news

## tables
- tx_crisisfeed_domain_model_news
- tx_crisisfeed_domain_model_currentnews
- tx_crisisfeed_domain_model_overlaynews

## annotations
- Find the documentation at https://tourstream.atlassian.net/wiki/display/TOUR/Crisisfeed+extension+for+TYPO3+%3E%3D+6.0


