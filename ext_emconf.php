<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "efempty".
 *
 * Auto generated 16-10-2014 10:23
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'Crisis News Feed',
	'description' => 'Imports the latest news from the central crisis news server and displays it',
	'category' => 'plugin',
	'version' => '1.1.0',
	'state' => 'stable',
	'uploadfolder' => 0,
	'createDirs' => 'fileadmin/crisisfeed',
	'clearcacheonload' => 0,
	'author' => 'Carsten Windler',
	'author_email' => 'carsten.windler@fti-e.com',
	'author_company' => 'fti',
	'constraints' => 
	array (
		'depends' => 
		array (
			'php' => '5.3.7-0.0.0',
			'typo3' => '6.0.0-6.2.99',
		),
		'conflicts' => 
		array (
		),
		'suggests' => 
		array (
		),
	),
);

