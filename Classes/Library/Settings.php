<?php

namespace Fti\Crisisfeed\Library;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Carsten Windler <carsten.windler@fti-ecom.de>, FTI eCom
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Trait which provides access to Settings & Configuration
 *
 * @author Carsten Windler <carsten.windler@fti-ecom.de>
 * @package crisisfeed
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
trait Settings
{
	/**
	 * The extensions name
	 * @var string
	 */
	protected $extensionName;
	/**
	 * Typo3 Settings array
	 * @var array
	 */
	protected $settings;

	/**
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
	 */
	protected $configurationManager;

	/**
	 * @param \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager
	 * @inject
	 */
	public function injectConfigurationManager(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager) {
		$this->configurationManager = $configurationManager;
	}

	/**
	 * Getter for $extensionName
	 * @return string
	 */
	public function getExtensionName() {
		if ($this->extensionName === NULL) {
			$className = get_class($this);
			if (strpos($className, '\\') !== FALSE) {
				$classNameParts = explode('\\', $className, 4);
				// Skip vendor and product name for core classes
				if (strpos($className, 'TYPO3\\CMS\\') === 0) {
					$this->extensionName = $classNameParts[2];
				} else {
					$this->extensionName = $classNameParts[1];
				}
			} else {
				list(, $this->extensionName) = explode('_', $className);
			}
		}

		return $this->extensionName;
	}

	/**
	 * Setter for $extensionName
	 * @param string $extensionName
	 * @return Settings
	 */
	public function setExtensionName($extensionName) {
		$this->extensionName = $extensionName;
		return $this;
	}

	/**
	 * Setter for $settings
	 *
	 * @param array $settings
	 *
	 * @return $this
	 */
	public function setSettings($settings) {
		$this->settings = $settings;

		return $this;
	}

	/**
	 * Getter for $settings
	 * If this is called outside Extbase (i.e. from a system hook, or a scheduler task) you must use
	 * \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT
	 * and fetch the extensions settings via the array structure, e.g.
	 * $this->settings['plugin.']['yourextension.']['persistence.']['storagePid']
	 *
	 * @param string $settingsType
	 * @return array
	 */
	public function getSettings($settingsType = \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS) {
		if ($this->settings === NULL) {
			// Get the settings as well
			$this->settings = $this->configurationManager->getConfiguration(
				$settingsType, $this->getExtensionName()
			);
		}

		return $this->settings;
	}
}