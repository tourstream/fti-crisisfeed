<?php
namespace Fti\Crisisfeed\Domain\Service;

use TYPO3\CMS\Core\Utility\GeneralUtility;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Carsten Windler <carsten.windler@fti-ecom.de>, FTI eCom
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * DataParser
 *
 * @author Carsten Windler <carsten.windler@fti-ecom.de>
 * @package crisisfeed
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class DataParser {

	/**
	 * RequestHeaderCreator
	 *
	 * @var \Fti\Crisisfeed\Domain\Service\RequestHeaderCreator
	 */
	protected $requestHeaderCreator;

	/**
	 * Injects the RequestHeaderCreator
	 * @param RequestHeaderCreator $requestHeaderCreator
	 * @return Importer
	 */
	public function injectRequestHeaderCreator(\Fti\Crisisfeed\Domain\Service\RequestHeaderCreator $requestHeaderCreator) {
		$this->requestHeaderCreator = $requestHeaderCreator;
		return $this;
	}

	/**
	 * A simple wrapper for the Typo3 getUrl(), which again wraps other functions...
	 * Was needed for proper unit testing
	 *
	 * @param $url
	 * @throws \Fti\Crisisfeed\Exception\RuntimeException
	 * @return object
	 */
	public function fetchUrlAsSimpleXml($url) {
		$httpAuthHeaderString = $this->requestHeaderCreator->createHttpAuthRequestHeaderString();
		$requestHeaders = FALSE;
		if ($httpAuthHeaderString){
			$requestHeaders = array(
				$httpAuthHeaderString
			);
		}

		$result = GeneralUtility::getUrl($url, 0, $requestHeaders);

		if (!$result) {
			throw new \Fti\Crisisfeed\Exception\RuntimeException('URL is not responding (' . $url . ')');
		}

		return simplexml_load_string(trim($result));
	}
}
