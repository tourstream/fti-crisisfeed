<?php

namespace Fti\Crisisfeed\Domain\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Martin Lukas <martin.lukas@tourstream.eu>, FTI tourstream
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * RequestHeaderCreator
 *
 * @author Martin Lukas <martin.lukas@tourstream.eu>
 * @package crisisfeed
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class RequestHeaderCreator implements \TYPO3\CMS\Core\SingletonInterface {
    use \Fti\Crisisfeed\Library\Settings;

    /**
     * If username and password are set in TS constants, returns the string for a HTTP Basic Auth Header like
     * 'Authorization:Basic adfadfekj33jjfajsdlDFa='
     *
     * Returns false if no credentials are set
     *
     * @return bool|string
     */
    public function createHttpAuthRequestHeaderString(){
        $settings = $this->getSettings( \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
        $httpAuthUser =  $settings['plugin.']['tx_crisisfeed.']['settings.']['client.']['httpAccessCredentials.']['user'];
        $httpAuthPw =  $settings['plugin.']['tx_crisisfeed.']['settings.']['client.']['httpAccessCredentials.']['password'];

        $httpAuthHeaderString = FALSE;
        if (!empty($httpAuthUser) && !empty($httpAuthPw)){
            $httpAuthHeaderString = 'Authorization:Basic '. base64_encode($httpAuthUser . ':' . $httpAuthPw);
        }

        return $httpAuthHeaderString;
    }
}