<?php
namespace Fti\Crisisfeed\Domain\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Carsten Windler <carsten.windler@fti-ecom.de>, FTI eCom
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Importer Service
 *
 * @author Carsten Windler <carsten.windler@fti-ecom.de>
 * @package crisisfeed
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class Importer extends ServiceAbstract
{
	/**
	 * DataParser
	 * @var \Fti\Crisisfeed\Domain\Service\DataParser
	 */
	protected $dataParser;

	/**
	 * RequestHeaderCreator
	 *
	 * @var \Fti\Crisisfeed\Domain\Service\RequestHeaderCreator
	 */
	protected $requestHeaderCreator;

	/**
	 * The feeds URL
	 * @var string
	 */
	protected $feedUrl = '';

	/**
	 * Injects the Data parser
	 * @param DataParser $dataParser
	 * @return Importer
	 */
	public function injectDataParser(\Fti\Crisisfeed\Domain\Service\DataParser $dataParser) {
		$this->dataParser = $dataParser;
		return $this;
	}

	/**
	 * Injects the RequestHeaderCreator
	 * @param RequestHeaderCreator $requestHeaderCreator
	 * @return Importer
	 */
	public function injectRequestHeaderCreator(\Fti\Crisisfeed\Domain\Service\RequestHeaderCreator $requestHeaderCreator) {
		$this->requestHeaderCreator = $requestHeaderCreator;
		return $this;
	}

	/**
	 * Returns the current file upload folder
	 * @return string
	 */
	public function getUploadFolder() {
		return $this->settings['settings.']['fileUploadPath'];
	}

	/**
	 * Setter for $feedUrl
	 * @return Importer
	 */
	protected function setFeedUrlFromConfiguration() {
		$server = $this->settings['settings.']['client.']['server'];
		$portalKey = $this->settings['settings.']['client.']['portalKey'];

		if (!isset($this->settings['settings.']['server.']['feed.']['locations.'][$server])) {
			// if this happens, somebody tweaked something without knowing what he/she does
			$this->log("Portal key " . $portalKey . " is not recognized", 3);

			return $this;
		}

		$protocol = $this->settings['settings.']['server.']['feed.']['protocol'];
		$location = $this->settings['settings.']['server.']['feed.']['locations.'][$server];
		$parameters = str_replace(
			'###PORTALKEY###',
			$portalKey,
			$this->settings['settings.']['server.']['feed.']['parameters.']['getActive']
		);

		$this->feedUrl = $protocol . $location . '?' . $parameters;

		return $this;
	}

	/**
	 * Reads complete Feed into an array
	 * @throws \Exception
	 * @return array
	 */
	protected function readFeedIntoArray() {
		$result = array();

		if (empty($this->feedUrl)) {
			throw new \Exception('feedUrl not configured');
		}

		$rss = $this->dataParser->fetchUrlAsSimpleXml($this->feedUrl);

		if (!isset($rss->channel)) {
			throw new \Exception('Feed does not seem to be an RSS Feed (' . $this->feedUrl . ')');
		}

		// iterate over all feed items
		foreach ($rss->channel->item as $item) {
			/** @var $newsItem \Fti\Crisisfeed\Domain\Model\CurrentNews */
			$newsItem = new \Fti\Crisisfeed\Domain\Model\CurrentNews;

			// getting <content:encoded> is a bit more effort
			$content = (string)$item->children("content", true);

			// Populate newsItem class
			$newsItem
				->setTitle((string)$item->title)
				->setHeader((string)$item->title)
				->setSubheader((string)$item->description)
				->setAuthor((string)$item->author)
				->setBodytext($content)
				->setDatetime(strtotime($item->pubDate))
				->setGuid((string)$item->guid);

			// Image processing
			if (isset($item->enclosure) && !empty($item->enclosure['url'])) {
				$newsItem->setImage((string)$item->enclosure['url']);
			}

			$result[] = $newsItem;
		}

		return $result;
	}

    /**
     * Reads complete Overlay Feed into an array
     * @throws \Exception
     * @return array
     */
    protected function readOverlayFeedIntoArray() {
        $result = array();

        if (empty($this->feedUrl)) {
            throw new \Exception('feedUrl not configured');
        }

        $rss = $this->dataParser->fetchUrlAsSimpleXml($this->feedUrl);

        if (!isset($rss->channel)) {
            throw new \Exception('Feed does not seem to be an RSS Feed (' . $this->feedUrl . ')');
        }

        // iterate over all feed items
        foreach ($rss->channel->overlayitem as $item) {
            /** @var $newsItem \Fti\Crisisfeed\Domain\Model\CurrentOverlayNews */
            $overlayItem = new \Fti\Crisisfeed\Domain\Model\CurrentOverlayNews();

            // getting <content:encoded> is a bit more effort
            $content = (string)$item->children("content", true);

            // Populate newsItem class
            $overlayItem
                ->setTitle((string)$item->title)
                ->setLive((string)$item->live)
                ->setHeader((string)$item->header)
                ->setSubheader((string)$item->subheader)
                ->setBodytext($content);

            $result[] = $overlayItem;
        }

        return $result;
    }

	/**
	 * Returns the current news item
	 * @return array
	 */
	public function getCurrentNewsItems() {
		return $this->setFeedUrlFromConfiguration()->readFeedIntoArray();
	}

    /**
     * Returns the current overlay news
     * @return array
     */
    public function getOverlayNewsItems() {
        return $this->setFeedUrlFromConfiguration()->readOverlayFeedIntoArray();
    }

	/**
	 * Import the image referenced in the news item(i.e. download it from the given server and store it locally)
	 * @param \Fti\Crisisfeed\Domain\Model\CurrentNews $newsItem
	 * @return bool
	 * @throws \Exception
	 */
	public function importImage(\Fti\Crisisfeed\Domain\Model\CurrentNews $newsItem) {
		$remoteImagePath = $newsItem->getImage();

		if (empty($remoteImagePath)) {
			// No image attached - nothing to do here
			return TRUE;
		}

		// Get the image name from the remote image URL
		$urlInfoArr = parse_url($remoteImagePath);
		$remoteImagePath = $urlInfoArr['path'];
		$pathInfoArr = pathinfo($remoteImagePath);
		$imageName = $pathInfoArr['basename'];
		$localImageWebPath = $this->getUploadFolder() . '/' . $imageName;
		$localImageFilePath = PATH_site . $localImageWebPath;

		// set HTTP Basic Auth Header if necessary
		$context = null;
		$httpAuthHeaderString = $this->requestHeaderCreator->createHttpAuthRequestHeaderString();
		if ($httpAuthHeaderString){
			$opts = array(
					'http'=>array(
							'header'=> $httpAuthHeaderString
					)
			);
			$context = stream_context_create($opts);
		}

		// download image
		$remoteImage = file_get_contents($newsItem->getImage(), null, $context);

		if (!$remoteImage) {
			throw new \Exception('Image ' . $newsItem->getImage() . 'referenced in News Item cannot be imported');
		}

		// and store it locally
		$result = file_put_contents($localImageFilePath, $remoteImage);

		if ($result === FALSE) {
			throw new \Exception('Image ' . $newsItem->getImage() . ' referenced in News Item cannot be imported to path ' . $localImageFilePath);
		}

		// set updated image path
		$newsItem->setImage($localImageWebPath);

		return TRUE;
	}
}