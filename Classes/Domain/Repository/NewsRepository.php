<?php

namespace Fti\Crisisfeed\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Carsten Windler <carsten.windler@fti-ecom.de>, FTI eCom
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Repository for News
 *
 * @author Carsten Windler <carsten.windler@fti-ecom.de>
 * @package crisisfeed
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class NewsRepository extends \TYPO3\CMS\Extbase\Persistence\Repository  {
	use \Fti\Crisisfeed\Library\Settings;

	/**
	 * Name of associated table
	 * @var string
	 */
	protected $tableName = 'tx_crisisfeed_domain_model_news';

	/**
	 * Return the property name for the given portal key
	 * @param $portalKey
	 * @return string
	 */
	protected function getPropertyNameFromPortalKey($portalKey) {
		return 'portal' . $portalKey;
	}

	/**
	 * Validate the given portal key. We consider it valid if the News Model has an according property
	 * @param $portalKey
	 * @return bool
	 */
	protected function validatePortalKey($portalKey) {
		return property_exists('\\Fti\\Crisisfeed\\Domain\\Model\\News', $this->getPropertyNameFromPortalKey($portalKey));
	}

	/**
	 * Find the current news which shall be shown for the given portal key
	 * @param $portalKey
	 * @return array
	 */
	public function findActiveNewsForPortalKey($portalKey) {
		$returnArr = NULL;

		if (!$this->validatePortalKey($portalKey)) {
			return $returnArr;
		}
		$query = $this->createQuery();
		// Tx_Extbase_Persistence_QueryInterface
		// We sort by date of publishing (descending)
		$query->setOrderings(
			array('datetime' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING)
		);

		// Take into account starttime and andtime
		$constraints = $query->logicalAnd(
			$query->equals($this->getPropertyNameFromPortalKey($portalKey), 1),
			$query->lessThanOrEqual('starttime', time()),
			$query->logicalOr(
				$query->greaterThan('endtime', time()),
				$query->equals('endtime', 0)
			)
		);

		$result = $query->matching($constraints)->execute();

		if ($result) {
			$settings = $this->getSettings();

			/** @var $newsitem \Fti\Crisisfeed\Domain\Model\News */
			foreach ($result as $newsitem) {
				// Set default author, if not set in news item itself
				if ($newsitem->getAuthor() == "") {
					if (isset($settings['newsitems']['defaultAuthor'])) {
						$newsitem->setAuthor($settings['newsitems']['defaultAuthor']);
					}
				}

				$returnArr[] = $newsitem;
			}
		}

		return $returnArr;
	}

	/**
	 * Set all non-hidden news which are expired as hidden and move them to the archvie folder
	 * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
	 */
	public function setExpiredNewsAsHidden() {
		$query = $this->createQuery();
		// Tx_Extbase_Persistence_QueryInterface

		// Select all non hidden items which are expired
		$constraints = $query->logicalAnd(
			$query->greaterThan('endtime', 0),
			$query->lessThan('endtime', time())
		);

		$result = $query->matching($constraints)->execute();

		if ($result) {
			$settings = $this->getSettings();

			/** @var $newsitem \Fti\Crisisfeed\Domain\Model\BaseNews */
			foreach ($result as $newsitem) {
				if (!$newsitem->getHidden()) {
					$newsitem->setHidden(true);

					// Move news to archive, if set
					if (!empty($settings['server.']['archiveFolderPid'])) {
						$newsitem->setPid($settings['server.']['archiveFolderPid']);
					}

					$this->update($newsitem);
				}
			}

			$this->persistenceManager->persistAll();
		}
	}
}