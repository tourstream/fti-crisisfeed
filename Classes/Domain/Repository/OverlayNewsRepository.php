<?php

namespace Fti\Crisisfeed\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Carsten Windler <carsten.windler@fti-ecom.de>, FTI eCom
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Repository for Overlay News
 *
 * @author Alessandro Bongiorni <alessandro.bongiorni@fti.de>
 * @package crisisfeed
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class OverlayNewsRepository extends \Fti\Crisisfeed\Domain\Repository\NewsRepository  {
    use \Fti\Crisisfeed\Library\Settings;

    /**
     * Name of associated table
     * @var string
     */
    protected $tableName = 'tx_crisisfeed_domain_model_overlaynews';

    /**
     * Importer service
     * @var \Fti\Crisisfeed\Domain\Service\Importer
     */
    protected $importer;

    /**
     * Inject the importer
     * @param \Fti\Crisisfeed\Domain\Service\Importer $importer
     * @return $this
     */
    public function injectImporter(\Fti\Crisisfeed\Domain\Service\Importer $importer) {
        $this->importer = $importer;

        return $this;
    }

    /**
     * Return the property name for the given portal key
     * @param $portalKey
     * @return string
     */
    protected function getPropertyNameFromPortalKey($portalKey) {
        return 'portal' . $portalKey;
    }

    /**
     * Validate the given portal key. We consider it valid if the News Model has an according property
     * @param $portalKey
     * @return bool
     */
    protected function validatePortalKey($portalKey) {
        return property_exists('\\Fti\\Crisisfeed\\Domain\\Model\\OverlayNews', $this->getPropertyNameFromPortalKey($portalKey));
    }

    /**
     * Find the overlay news which shall be shown
     * @param $portalKey
     * @return array
     */
    public function findActiveOverlays($portalKey) {

        $returnArr = NULL;

        if (!$this->validatePortalKey($portalKey)) {
            return $returnArr;
        }

        $returnArr = NULL;

        $query = $this->createQuery();
        // Tx_Extbase_Persistence_QueryInterface

        $constraints = $query->logicalAnd(
            $query->logicalAnd(
                $query->equals('deleted', 0),
                $query->equals('hidden', 0),
                $query->equals('live', 1),
                $query->equals($this->getPropertyNameFromPortalKey($portalKey), 1),
                $query->lessThanOrEqual('starttime', time()),
                $query->logicalOr(
                    $query->greaterThan('endtime', time()),
                    $query->equals('endtime', 0)
                )
            )
        );

        $result = $query->matching($constraints)->execute();

        if ($result) {
            /** @var $newsitem \Fti\Crisisfeed\Domain\Model\OverlayNews */
            foreach ($result as $overlayitem) {
                $returnArr[] = $overlayitem;
            }
        }

        return $returnArr;
    }
}