<?php

namespace Fti\Crisisfeed\Domain\Repository;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Carsten Windler <carsten.windler@fti-ecom.de>, FTI eCom
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Repository for Current News
 *
 * @author Carsten Windler <carsten.windler@fti-ecom.de>
 * @package crisisfeed
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class CurrentNewsRepository extends \TYPO3\CMS\Extbase\Persistence\Repository  {
	use \Fti\Crisisfeed\Library\Settings;

	/**
	 * Importer service
	 * @var \Fti\Crisisfeed\Domain\Service\Importer
	 */
	protected $importer;

	/**
	 * Inject the importer
	 * @param \Fti\Crisisfeed\Domain\Service\Importer $importer
	 * @return $this
	 */
	public function injectImporter(\Fti\Crisisfeed\Domain\Service\Importer $importer) {
		$this->importer = $importer;

		return $this;
	}

	/**
	 * Upa
	 * @param $newsItems
	 * @return boolean
	 * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
	 */
	public function updateLocalQueue($newsItems) {
		$itemByGuidArr = array();
		$hasUpdated = false;

		// build a temporary array which makes processing for us easier
		foreach ($newsItems as $newsItem) {
			$itemByGuidArr[$newsItem->getGuid()] = $newsItem;
		}

		foreach ($this->findAll() as $localItem) {
			// Remove all local items which are not amongst the list we got
			$localGuid = $localItem->getGuid();

			if (isset($itemByGuidArr[$localGuid])) {
				unset($itemByGuidArr[$localGuid]);
			} else {
				$this->remove($localItem);
				$hasUpdated = true;
			}
		}

		// We got any guids over? These are new items, so add them
		if (count($itemByGuidArr) > 0) {
			foreach ($itemByGuidArr as $newsItem) {
				$this->importItem($newsItem);
				$hasUpdated = true;
			}
		}

		return $hasUpdated;
	}

	/**
	 * Add item to our queue. Also import the image, if any
	 * @param $newsItem
	 */
	protected function importItem($newsItem) {
		$settings = $this->getSettings();
		$storagePid = $settings['plugin.']['tx_crisisfeed.']['persistence.']['storagePid'];

		if ($this->importer->importImage($newsItem)) {
			$newsItem->setPid($storagePid);
			$this->add($newsItem);
		}
	}
}