<?php

namespace Fti\Crisisfeed\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Carsten Windler <carsten.windler@fti-ecom.de>, FTI eCom
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Repository for the current News item
 *
 * @author Carsten Windler <carsten.windler@fti-ecom.de>
 * @package crisisfeed
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class CurrentNews extends \Fti\Crisisfeed\Domain\Model\BaseNews {

	/**
	 * Date/Time of publishing
	 * @var integer
	 */
	protected $datetime;

	/**
	 * The author
	 * @var string
	 */
	protected $author;

	/**
	 * The image as base64 encoded string
	 * @var string
	 */
	protected $image;

	/**
	 * Getter for $author
	 * @return string
	 */
	public function getAuthor() {
		return $this->author;
	}

	/**
	 * Setter for $author
	 * @param string $author
	 * @return News
	 */
	public function setAuthor($author) {
		$this->author = $author;
		return $this;
	}

	/**
	 * Getter for $datetime
	 * @return int
	 */
	public function getDatetime() {
		return $this->datetime;
	}

	/**
	 * Setter for $datetime
	 * @param int $datetime
	 * @return News
	 */
	public function setDatetime($datetime) {
		$this->datetime = $datetime;
		return $this;
	}

	/**
	 * Getter for $image
	 * @return string
	 */
	public function getImage() {
		return $this->image;
	}

	/**
	 * Setter for $image
	 * @param string $image
	 * @return News
	 */
	public function setImage($image) {
		$this->image = $image;
		return $this;
	}

	/**
	 * Return an array which contains the most important object properties
	 * used for e-zy-compare (R)
	 * @return array
	 */
	public function getDataArray() {
		return array(
			'header' => $this->getHeader(),
			'title' => $this->getTitle(),
			'bodytext' => $this->getBodytext(),
			'subheader' => $this->getSubheader(),
			'author' => $this->getAuthor(),
			'datetime' => $this->getDatetime(),
			'image' => $this->getImage(),
		);
	}

}
