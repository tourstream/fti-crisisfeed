<?php

namespace Fti\Crisisfeed\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2016 Martin Lukas <martin.lukas@tourstream.eu>, FTI tourstream
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Repository for the current News item
 *
 * @author Martin Lukas <martin.lukas@tourstream.eu>
 * @package crisisfeed
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
abstract class BaseNews extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {
	/**
	 * The news title (i.e. for the record item, not published via feed)
	 * @var string
	 */
	protected $title;

	/**
	 * Hidden state
	 * @var boolean
	 */
	protected $hidden;

	/**
	 * The news header
	 * @var string
	 */
	protected $header;

	/**
	 * The news subheader
	 * @var string
	 */
	protected $subheader;

	/**
	 * The news bodytext
	 * @var string
	 */
	protected $bodytext;

	/**
	 * Date/Time of publishing
	 * @var integer
	 */
	protected $datetime;

	/**
	 * Globally unique identifier (well, maybe not *globally*, but within our tiny domain.
	 * It is a md5 hash over the most important object properties
	 * @var string
	 */
	protected $guid;

	/**
	 * Getter for $guid
	 * @return string
	 */
	public function getGuid() {
		if (empty($this->guid)) {
			$stringToHash = implode('', $this->getDataArray());

			if (!empty($stringToHash)) {
		   		$this->guid = md5($stringToHash);
			}
		}

		return $this->guid;
	}

	/**
	 * Setter for $guid
	 * @param string $guid
	 * @return CurrentNews
	 */
	public function setGuid($guid) {
		$this->guid = $guid;
		return $this;
	}

	/**
	 * Getter for $bodytext
	 * @return string
	 */
	public function getBodytext() {
		return $this->bodytext;
	}

	/**
	 * Setter for $bodytext
	 * @param string $bodytext
	 * @return News
	 */
	public function setBodytext($bodytext) {
		$this->bodytext = $bodytext;
		return $this;
	}

	/**
	 * Getter for $datetime
	 * @return int
	 */
	public function getDatetime() {
		return $this->datetime;
	}

	/**
	 * Setter for $datetime
	 * @param int $datetime
	 * @return News
	 */
	public function setDatetime($datetime) {
		$this->datetime = $datetime;
		return $this;
	}

	/**
	 * Getter for $header
	 * @return string
	 */
	public function getHeader() {
		return $this->header;
	}

	/**
	 * Setter for $header
	 * @param string $header
	 * @return News
	 */
	public function setHeader($header) {
		$this->header = $header;
		return $this;
	}

	/**
	 * Getter for $subheader
	 * @return string
	 */
	public function getSubheader() {
		return $this->subheader;
	}

	/**
	 * Setter for $subheader
	 * @param string $subheader
	 * @return News
	 */
	public function setSubheader($subheader) {
		$this->subheader = $subheader;
		return $this;
	}

	/**
	 * Getter for $title
	 * @return string
	 */
	public function getTitle() {
		return $this->title;
	}

	/**
	 * Setter for $title
	 * @param string $title
	 * @return News
	 */
	public function setTitle($title) {
		$this->title = $title;
		return $this;
	}

	/**
	 * Getter for $hidden
	 * @return boolean
	 */
	public function getHidden() {
		return $this->hidden;
	}

	/**
	 * Setter for $hidden
	 * @param boolean $hidden
	 * @return CurrentNews
	 */
	public function setHidden($hidden) {
		$this->hidden = $hidden;
		return $this;
	}

	abstract function getDataArray();
}
