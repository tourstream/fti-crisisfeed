<?php
namespace Fti\Crisisfeed\Domain\Model;

trait PortalSpecs {
    /**
     * Whether the news is to appear for the portal key
     * @var integer
     */
    protected $portalTest;

    /**
     * Whether the news is to appear for the portal key
     * @var integer
     */
    protected $portalFtiDe;

    /**
     * Whether the news is to appear for the portal key
     * @var integer
     */
    protected $portalFtiCh;

    /**
     * Whether the news is to appear for the portal key
     * @var integer
     */
    protected $portalFtiAt;

    /**
     * Whether the news is to appear for the portal key
     * @var integer
     */
    protected $portal5vorflugDe;

    /**
     * Whether the news is to appear for the portal key
     * @var integer
     */
    protected $portalSonnenklarTv;

    /**
     * Whether the news is to appear for the portal key
     * @var integer
     */
    protected $portalBigextraDe;

    /**
     * Whether the news is to appear for the portal key
     * @var integer
     */
    protected $portalLalDe;

    /**
     * Whether the news is to appear for the portal key
     * @var integer
     */
    protected $portalFtiCruisesCom;

    /**
     * Getter for $portal5vorflugDe
     * @return int
     */
    public function getPortal5vorflugDe() {
        return $this->portal5vorflugDe;
    }

    /**
     * Setter for $portal5vorflugDe
     * @param int $portal5vorflugDe
     * @return News
     */
    public function setPortal5vorflugDe($portal5vorflugDe) {
        $this->portal5vorflugDe = $portal5vorflugDe;
        return $this;
    }

    /**
     * Getter for $portalBigextraDe
     * @return int
     */
    public function getPortalBigextraDe() {
        return $this->portalBigextraDe;
    }

    /**
     * Setter for $portalBigextraDe
     * @param int $portalBigextraDe
     * @return News
     */
    public function setPortalBigextraDe($portalBigextraDe) {
        $this->portalBigextraDe = $portalBigextraDe;
        return $this;
    }

    /**
     * Getter for $portalFtiAt
     * @return int
     */
    public function getPortalFtiAt() {
        return $this->portalFtiAt;
    }

    /**
     * Setter for $portalFtiAt
     * @param int $portalFtiAt
     * @return News
     */
    public function setPortalFtiAt($portalFtiAt) {
        $this->portalFtiAt = $portalFtiAt;
        return $this;
    }

    /**
     * Getter for $portalFtiCh
     * @return int
     */
    public function getPortalFtiCh() {
        return $this->portalFtiCh;
    }

    /**
     * Setter for $portalFtiCh
     * @param int $portalFtiCh
     * @return News
     */
    public function setPortalFtiCh($portalFtiCh) {
        $this->portalFtiCh = $portalFtiCh;
        return $this;
    }

    /**
     * Getter for $portalFtiCruisesCom
     * @return int
     */
    public function getPortalFtiCruisesCom() {
        return $this->portalFtiCruisesCom;
    }

    /**
     * Setter for $portalFtiCruisesCom
     * @param int $portalFtiCruisesCom
     * @return News
     */
    public function setPortalFtiCruisesCom($portalFtiCruisesCom) {
        $this->portalFtiCruisesCom = $portalFtiCruisesCom;
        return $this;
    }

    /**
     * Getter for $portalFtiDe
     * @return int
     */
    public function getPortalFtiDe() {
        return $this->portalFtiDe;
    }

    /**
     * Setter for $portalFtiDe
     * @param int $portalFtiDe
     * @return News
     */
    public function setPortalFtiDe($portalFtiDe) {
        $this->portalFtiDe = $portalFtiDe;
        return $this;
    }

    /**
     * Getter for $portalLalDe
     * @return int
     */
    public function getPortalLalDe() {
        return $this->portalLalDe;
    }

    /**
     * Setter for $portalLalDe
     * @param int $portalLalDe
     * @return News
     */
    public function setPortalLalDe($portalLalDe) {
        $this->portalLalDe = $portalLalDe;
        return $this;
    }

    /**
     * Getter for $portalSonnenklarTv
     * @return int
     */
    public function getPortalSonnenklarTv() {
        return $this->portalSonnenklarTv;
    }

    /**
     * Setter for $portalSonnenklarTv
     * @param int $portalSonnenklarTv
     * @return News
     */
    public function setPortalSonnenklarTv($portalSonnenklarTv) {
        $this->portalSonnenklarTv = $portalSonnenklarTv;
        return $this;
    }

    /**
     * Getter for $portalTest
     * @return int
     */
    public function getPortalTest() {
        return $this->portalTest;
    }

    /**
     * Setter for $portalTest
     * @param int $portalTest
     * @return News
     */
    public function setPortalTest($portalTest) {
        $this->portalTest = $portalTest;
        return $this;
    }
}