<?php

namespace Fti\Crisisfeed\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Carsten Windler <carsten.windler@fti-ecom.de>, FTI eCom
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Repository for the current News item
 *
 * @author Alessandro Bongiorni <alessandro.bongiorni@fti.de>
 * @package crisisfeed
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class CurrentOverlayNews extends \Fti\Crisisfeed\Domain\Model\BaseNews {

    /**
     * Live state
     * @var boolean
     */
    protected $live;

    /**
     * The news startDate
     * @var int
     */
    protected $startDate;

    /**
     * The news endDate
     * @var int
     */
    protected $endDate;

	/**
     * Getter for $live
     * @return boolean
     */
    public function getLive() {
        return $this->live;
    }

    /**
     * Setter for $live
     * @param boolean $live
     * @return OverlayNews
     */
    public function setLive($live) {
        $this->live = $live;
        return $this;
    }

    /**
     * Getter for $startDate
     * @return int
     */
    public function getStartDate() {
        return $this->startDate;
    }

    /**
     * Setter for $startDate
     * @param int $startDate
     * @return OverlayNews
     */
    public function setStartDate($startDate) {
        $this->startDate = $startDate;
        return $this;
    }

    /**
     * Getter for $endDate
     * @return int
     */
    public function getEndDate() {
        return $this->endDate;
    }

    /**
     * Setter for $endDate
     * @param int $endDate
     * @return OverlayNews
     */
    public function setEndDate($endDate) {
        $this->endDate = $endDate;
        return $this;
    }

	/**
	 * Return an array which contains the most important object properties
	 * used for e-zy-compare (R)
	 * @return array
	 */
	public function getDataArray() {
		return array(
			'header' => $this->getHeader(),
			'title' => $this->getTitle(),
			'bodytext' => $this->getBodytext(),
			'subheader' => $this->getSubheader(),
		);
	}

}
