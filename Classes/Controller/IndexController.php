<?php

namespace Fti\Crisisfeed\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Carsten Windler <carsten.windler@fti-ecom.de>, FTI eCom
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Index Controller
 *
 * @author Carsten Windler <carsten.windler@fti-ecom.de>
 * @package crisisfeed
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class IndexController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {
	/**
	 * News Repository
	 *
	 * @var \Fti\Crisisfeed\Domain\Repository\NewsRepository
	 * @inject
	 */
	protected $newsRepository;

    /**
     * Overlay News Repository
     *
     * @var \Fti\Crisisfeed\Domain\Repository\OverlayNewsRepository
     * @inject
     */
    protected $overlayRepository;

	/**
	 * Index action for this controller.
	 *
	 * @return string The rendered view
	 */
	public function indexAction() {
		// nothing to do here atm
	}

	/**
	 * Display the actuve
	 * @param string $portalkey
	 */
	public function getActiveRssFeedAction($portalkey = '') {
		if (empty($portalkey)) {
			// log invalid request
		}

		$newsItems = $this->newsRepository->findActiveNewsForPortalKey($portalkey);
        $overlayItems = $this->overlayRepository->findActiveOverlays($portalkey);

		/* todo
		if (!$news) {
			$this->redirect('error');
		}
		*/

		$this->view->assign('sitetitle', $this->settings['server']['title']);
		$this->view->assign('sitelanguage', $this->settings['server']['language']);
		$this->view->assign('sitedescription', sprintf($this->settings['server']['description'], $portalkey));
		$this->view->assign('sitelastbuilddate', time());
		$this->view->assign('newsitems', $newsItems);
        $this->view->assign('overlayitems', $overlayItems);
	}

	public function errorAction() {
		die("!!!");
	}
}
