<?php

namespace Fti\Crisisfeed\Task;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Carsten Windler <carsten.windler@fti-ecom.de>, FTI eCom
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Importer Task
 *
 * @author Carsten Windler <carsten.windler@fti-ecom.de>
 * @package crisisfeed
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class Importer extends AbstractTask {

	/**
	 * relative path + filename where imported overlay news are stored
     */
	const OVERLAY_NEWS_FILE_PATH = 'fileadmin/crisisfeed/overlay.html';

	/**
	 * Importer service
	 * @var \Fti\Crisisfeed\Domain\Service\Importer
	 */
	protected $importer;

	/**
	 * Current News Repository
	 * @var \Fti\Crisisfeed\Domain\Repository\CurrentNewsRepository
	 */
	protected $currentNewsRepository;

	/**
	 * Execute the import scheduler
	 *
	 * tx_scheduler_Task
	 *
	 * @return bool
	 * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
	 */
	public function execute() {
		$this->init();
		$this->importCrisisNews();
		$this->importOverlayNews();

		return true;
	}

	/**
	 * Init needed stuff for running the Repository and Importer
	 * @return void
	 */
	protected function init() {
		parent::init();

		$this->importer = $this->objectManager->get('Fti\\Crisisfeed\\Domain\\Service\\Importer');

		$this->currentNewsRepository = $this->objectManager->get('Fti\\Crisisfeed\\Domain\\Repository\\CurrentNewsRepository');
		$this->currentNewsRepository->setSettings($this->settings);
		$this->currentNewsRepository->setDefaultQuerySettings($this->querySettings);
		$this->currentNewsRepository->injectImporter($this->importer);
	}

	/**
	 * import "normal" crisis news into DB
     */
	protected function importCrisisNews()
	{
		/** @var $currentNewsItem \Fti\Crisisfeed\Domain\Model\CurrentNews */
		$newsFromServer = $this->importer
				->setSettings($this->settings['plugin.']['tx_crisisfeed.'])
				->getCurrentNewsItems();

		// Remove all local (i.e. client side) news item which are not included in the new ons
		$isCrisisNewsUpdated = $this->currentNewsRepository->updateLocalQueue($newsFromServer);

		if ($isCrisisNewsUpdated) {
			$this->persistanceManager->persistAll();
			$this->clearPageCache();
		}
	}

	/**
	 *  import overlay crisis news into static file
     */
	protected function importOverlayNews()
	{
		/** @var $overlayNewsItem \Fti\Crisisfeed\Domain\Model\OverlayNews */
		$overlayFromServer = $this->importer
				->setSettings($this->settings['plugin.']['tx_crisisfeed.'])
				->getOverlayNewsItems();

		// without any active overlay news just write empty file
		if (empty($overlayFromServer)){
			$result = '';
		}
		// render overlay news template with current overlayNews
		else {
			$view = $this->objectManager->get('TYPO3\\CMS\\Fluid\\View\\StandaloneView');
			$templatePath = $this->settings['plugin.']['tx_crisisfeed.']['view.']['templateRootPath'];
			$view->setTemplatePathAndFilename(\TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($templatePath . 'Overlay/Index.html'));
			$layoutPath = $this->settings['plugin.']['tx_crisisfeed.']['view.']['layoutRootPath'];
			$view->setLayoutRootPath(\TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($layoutPath));
			$view->assign('overlayItems', $overlayFromServer);
			$view->assign('isPreview', 0);
			$result = $view->render();
		}

		$written = @file_put_contents(PATH_site . self::OVERLAY_NEWS_FILE_PATH , print_r($result, true));
		if ($written === false){
			throw new \Exception('Couldn\'t write static overlay news file at: ' . self::OVERLAY_NEWS_FILE_PATH);
		}
	}

	/**
	 * Clear the page cache for the configured pages
	 * @return Importer
	 */
	protected function clearPageCache() {
		if (!empty($this->settings['plugin.']['tx_crisisfeed.']['settings.']['client.']['clearCachePids'])) {
			$pids = $this->settings['plugin.']['tx_crisisfeed.']['settings.']['client.']['clearCachePids'];

			// Setting $pageIdsToClear to NULL will delete the whole page cache
			if ($pids == 'all') {
				$pageIdsToClear = null;
			} else {
				$pageIdsToClear = $this->getPidArrayFromString($pids);
			}

			/** @var $cacheService \TYPO3\CMS\Extbase\Service\CacheService */
			$cacheService = $this->objectManager->get('\\TYPO3\\CMS\\Extbase\\Service\\CacheService');
			$cacheService->clearPageCache($pageIdsToClear);
		}

		// This is not 100%ly a real Typo3 Hook, but it serves our needs
		// This allows us to call an additional cache clear function which might be portal specific
		// e.g. 5vorFlug -> ibeclearcache
		if (!empty($this->settings['plugin.']['tx_crisisfeed.']['settings.']['client.']['clearCacheHook'])) {
			$functionReference = $this->settings['plugin.']['tx_crisisfeed.']['settings.']['client.']['clearCacheHook'];
			$parameters = array('parentObject' => $this);

			\TYPO3\CMS\Core\Utility\GeneralUtility::callUserFunction($functionReference, $parameters, $this);
		}

		return $this;
	}
}