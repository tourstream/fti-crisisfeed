<?php

namespace Fti\Crisisfeed\Task;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Carsten Windler <carsten.windler@fti-ecom.de>, FTI eCom
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Abstract Class for Importer
 *
 * @author Carsten Windler <carsten.windler@fti-ecom.de>
 * @package crisisfeed
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
abstract class AbstractTask extends \TYPO3\CMS\Scheduler\Task\AbstractTask {
	/**
	 * The object manager
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 */
	protected $objectManager;

	/**
	 * The Extensions setting array
	 * @var array
	 */
	protected $settings;

	/**
	 * Persistance Manager
	 * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
	 */
	protected $persistanceManager;

	/**
	 * StoragePID
	 * @var integer
	 */
	protected $storagePid;

	/**
	 * Query settings object for Extbase
	 * @var
	 */
	protected $querySettings;

	/**
	 * Init needed stuff
	 * @return void
	 */
	protected function init() {
		//
		// A lot of stuff to do manually, thanks Extbase for great scheduler support
		//
		$this->objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');

		$configurationManager = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');
		$this->settings = $configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);

		$this->storagePid = $this->settings['plugin.']['tx_crisisfeed.']['persistence.']['storagePid'];

		// Tx_Extbase_Persistence_Typo3QuerySettings
		/** @var $querySettings \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings */
		$this->querySettings = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\Typo3QuerySettings');
		$this->querySettings->setStoragePageIds(array($this->storagePid));

		/** @var $persistanceManager \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager */
		$this->persistanceManager = $this->objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager');
	}

	/**
	 * Convert string of PIDs (comma separated) into array, deal with blanks between commas also
	 * @param $pidString
	 * @return array
	 */
	protected function getPidArrayFromString($pidString) {
		$resultArr = array();

		foreach(explode(',', $pidString) as $key => $value) {
			$resultArr[] = trim($value);
		}

		return $resultArr;
	}
}