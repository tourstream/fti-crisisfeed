<?php

namespace Fti\Crisisfeed\Task;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Carsten Windler <carsten.windler@fti-ecom.de>, FTI eCom
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Cleanup Task
 *
 * @author Carsten Windler <carsten.windler@fti-ecom.de>
 * @package crisisfeed
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class Cleanup extends AbstractTask {
	/**
	 * News Repository
	 * @var \Fti\Crisisfeed\Domain\Repository\NewsRepository
	 */
	protected $newsRepository;

	/**
	 * Overlay News Repository
	 * @var \Fti\Crisisfeed\Domain\Repository\OverlayNewsRepository
	 */
	protected $overlayNewsRepository;

	/**
	 * Execute the import scheduler
	 *
	 * tx_scheduler_Task
	 *
	 * @return bool
	 * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
	 */
	public function execute() {
		$this->init();

		// Check all news and set them to hidden, if they are expired
		$this->newsRepository->setExpiredNewsAsHidden();
		$this->overlayNewsRepository->setExpiredNewsAsHidden();

		return true;
	}

	/**
	 * Init needed stuff for running the Repository
	 * @return void
	 */
	public function init() {
		parent::init();

		$this->newsRepository = $this->objectManager->get('Fti\\Crisisfeed\\Domain\\Repository\\NewsRepository');
		$this->newsRepository->setSettings($this->settings['plugin.']['tx_crisisfeed.']['settings.']);
		$this->newsRepository->setDefaultQuerySettings($this->querySettings);

		$this->overlayNewsRepository = $this->objectManager->get('Fti\\Crisisfeed\\Domain\\Repository\\OverlayNewsRepository');
		$this->overlayNewsRepository->setSettings($this->settings['plugin.']['tx_crisisfeed.']['settings.']);
		$this->overlayNewsRepository->setDefaultQuerySettings($this->querySettings);
	}
}