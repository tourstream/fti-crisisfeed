<?php

namespace Fti\Crisisfeed\Hook;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Carsten Windler <carsten.windler@fti-ecom.de>, FTI eCom
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Hook into the Datamap processing
 *
 * @author Carsten Windler <carsten.windler@fti-ecom.de>
 * @package crisisfeed
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class NewsSave {
	use \Fti\Crisisfeed\Library\Settings;

	/**
	 * Hook into the Datamap processing
	 * We want to move records into the archvie folder when they are set to "hidden",
	 * and eventually back into the storage folder when they are set to "shown"
	 *
	 * @param $status
	 * @param $table
	 * @param $id
	 * @param $fieldArray
	 * @param $reference
	 */
	// public function processDatamap_preProcessFieldArray(array &$incomingFieldArray, $table, $id, &$reference) {
	public function processDatamap_postProcessFieldArray($status, $table, $id, &$fieldArray, &$reference) {
		// Hyper feature for the Krisenmanagement: move the record to another folder, when hidden = 1
		if ($table == 'tx_crisisfeed_domain_model_news' || $table == 'tx_crisisfeed_domain_model_overlaynews') {
			if (isset($fieldArray['hidden'])) {
				$objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');
				$configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManagerInterface');
				$this->injectConfigurationManager($configurationManager);

				// Get the full typoscript settings here, because we're not within the Extbase extension (technically)
				$settings = $this->getSettings(
					\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT
				);

				// If the new state of hidden is 1, move the news into the archive folder (if configured)
				if ($fieldArray['hidden'] == 1) {
					if (!empty($settings['plugin.']['tx_crisisfeed.']['settings.']['server.']['archiveFolderPid'])) {
						$fieldArray['pid'] = $settings['plugin.']['tx_crisisfeed.']['settings.']['server.']['archiveFolderPid'];
					}
				// otherwise move it back into the storage folder, as it is marked as "unhidden" aka shown
				} else {
					if (!empty($settings['plugin.']['tx_crisisfeed.']['persistence.']['storagePid'])) {
						$fieldArray['pid'] = $settings['plugin.']['tx_crisisfeed.']['persistence.']['storagePid'];
					}
				}
			}
		}
	}
}