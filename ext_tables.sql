#
# Table structure for table 'tx_crisisfeed_domain_model_news'
#
CREATE TABLE tx_crisisfeed_domain_model_news (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(30) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l18n_parent int(11) DEFAULT '0' NOT NULL,
	l18n_diffsource mediumtext,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	starttime int(11) DEFAULT '0' NOT NULL,
	endtime int(11) DEFAULT '0' NOT NULL,
  lastupdated int(11) DEFAULT '0' NOT NULL,
	title tinytext,
	header tinytext,
	subheader mediumtext,
	bodytext text,
	datetime int(11) DEFAULT '0' NOT NULL,
	author tinytext,
	image text,

	portal_test tinyint(4) DEFAULT '0' NOT NULL,
	portal_fti_de tinyint(4) DEFAULT '0' NOT NULL,
	portal_fti_ch tinyint(4) DEFAULT '0' NOT NULL,
	portal_fti_at tinyint(4) DEFAULT '0' NOT NULL,
	portal_5vorflug_de tinyint(4) DEFAULT '0' NOT NULL,
	portal_sonnenklar_tv tinyint(4) DEFAULT '0' NOT NULL,
	portal_bigextra_de tinyint(4) DEFAULT '0' NOT NULL,
	portal_lal_de tinyint(4) DEFAULT '0' NOT NULL,
	portal_fti_cruises_com tinyint(4) DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid),
	
	KEY parent (pid)
);

#
# Table structure for table 'tx_crisisfeed_domain_model_currentnews'
#
CREATE TABLE tx_crisisfeed_domain_model_currentnews (
  uid int(11) NOT NULL auto_increment,
  pid int(11) DEFAULT '0' NOT NULL,
  tstamp int(11) DEFAULT '0' NOT NULL,
  crdate int(11) DEFAULT '0' NOT NULL,
  cruser_id int(11) DEFAULT '0' NOT NULL,
  deleted tinyint(4) DEFAULT '0' NOT NULL,
  hidden tinyint(4) DEFAULT '0' NOT NULL,

  title tinytext,
  header tinytext,
  subheader mediumtext,
  bodytext text,
  datetime int(11) DEFAULT '0' NOT NULL,
  author tinytext,
  image text,
  guid tinytext,

  PRIMARY KEY (uid),

  KEY parent (pid)
);

#
# Table structure for table 'tx_crisisfeed_domain_model_overlaynews'
#
CREATE TABLE tx_crisisfeed_domain_model_overlaynews (
	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,
	tstamp int(11) DEFAULT '0' NOT NULL,
	crdate int(11) DEFAULT '0' NOT NULL,
	cruser_id int(11) DEFAULT '0' NOT NULL,
	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(30) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3_origuid int(11) DEFAULT '0' NOT NULL,
	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l18n_parent int(11) DEFAULT '0' NOT NULL,
	l18n_diffsource mediumtext,
	deleted tinyint(4) DEFAULT '0' NOT NULL,
	hidden tinyint(4) DEFAULT '0' NOT NULL,
	starttime int(11) DEFAULT '0' NOT NULL,
	endtime int(11) DEFAULT '0' NOT NULL,
  lastupdated int(11) DEFAULT '0' NOT NULL,
	title tinytext,
	header tinytext,
	subheader mediumtext,
	live tinyint(4) default NULL,
	bodytext text,
	datetime int(11) DEFAULT '0' NOT NULL,
	author tinytext,
	image text,

	portal_test tinyint(4) DEFAULT '0' NOT NULL,
	portal_fti_de tinyint(4) DEFAULT '0' NOT NULL,
	portal_fti_ch tinyint(4) DEFAULT '0' NOT NULL,
	portal_fti_at tinyint(4) DEFAULT '0' NOT NULL,
	portal_5vorflug_de tinyint(4) DEFAULT '0' NOT NULL,
	portal_sonnenklar_tv tinyint(4) DEFAULT '0' NOT NULL,
	portal_bigextra_de tinyint(4) DEFAULT '0' NOT NULL,
	portal_lal_de tinyint(4) DEFAULT '0' NOT NULL,
	portal_fti_cruises_com tinyint(4) DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid),

	KEY parent (pid)
);