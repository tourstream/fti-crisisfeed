<?php
namespace Fti\Crisisfeed\Tests\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Carsten Windler <carsten.windler@fti-ecom.de>, FTI eCom
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test Case for the News Model
 *
 * @author Carsten Windler <carsten.windler@fti-ecom.de>
 * @package crisisfeed
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class CurrentNewsTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	/**
	 * @test
	 */
	public function currentNewsCanBeInstanciatedAndPopulated() {
		$news = new \Fti\Crisisfeed\Domain\Model\CurrentNews();

		$dateTime = time();

		$news
			->setAuthor('authorname')
			->setBodytext('bodytext')
			->setDatetime($dateTime)
			->setHeader('header')
			->setImage('path/to/image')
			->setSubheader('subheader')
			->setTitle('title')
			->setGuid('guid')
			->setPid(3);

		$this->assertEquals('authorname', $news->getAuthor());
		$this->assertEquals('bodytext', $news->getBodytext());
		$this->assertEquals($dateTime, $news->getDatetime());
		$this->assertEquals('header', $news->getHeader());
		$this->assertEquals('path/to/image', $news->getImage());
		$this->assertEquals('subheader', $news->getSubheader());
		$this->assertEquals('title', $news->getTitle());
		$this->assertEquals(3, $news->getPid());
		$this->assertEquals('guid', $news->getGuid());
	}
}