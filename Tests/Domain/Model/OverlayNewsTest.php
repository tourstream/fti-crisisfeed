<?php

namespace Fti\Crisisfeed\Tests\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Carsten Windler <carsten.windler@fti-ecom.de>, FTI eCom
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Test Case for the News Model
 *
 * @author Carsten Windler <carsten.windler@fti-ecom.de>
 * @package crisisfeed
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class OverlayNewsTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	/**
	 * @test
	 */
	public function overlaynewsCanBeInstanciatedAndPopulated() {
		$overlayNews = new \Fti\Crisisfeed\Domain\Model\OverlayNews();

		$overlayNews
            ->setStartDate(1455114840)
            ->setEndDate(1455114840)
            ->setTitle('title')
            ->setHeader('header')
            ->setSubheader('subheader')
			->setBodytext('bodytext')
			->setPortal5vorflugDe(1)
			->setPortalBigextraDe(1)
			->setPortalFtiAt(1)
			->setPortalFtiCh(1)
			->setPortalFtiCruisesCom(1)
			->setPortalFtiDe(1)
			->setPortalLalDe(1)
			->setPortalSonnenklarTv(1)
			->setPortalTest(1);

		$this->assertEquals(1455114840, $overlayNews->getStartDate());
        $this->assertEquals(1455114840, $overlayNews->getEndDate());
        $this->assertEquals('title', $overlayNews->getTitle());
        $this->assertEquals('header', $overlayNews->getHeader());
        $this->assertEquals('subheader', $overlayNews->getSubheader());
		$this->assertEquals('bodytext', $overlayNews->getBodytext());
		$this->assertEquals(1, $overlayNews->getPortal5vorflugDe());
		$this->assertEquals(1, $overlayNews->getPortalBigextraDe());
		$this->assertEquals(1, $overlayNews->getPortalFtiAt());
		$this->assertEquals(1, $overlayNews->getPortalFtiCh());
		$this->assertEquals(1, $overlayNews->getPortalFtiCruisesCom());
		$this->assertEquals(1, $overlayNews->getPortalFtiDe());
		$this->assertEquals(1, $overlayNews->getPortalLalDe());
		$this->assertEquals(1, $overlayNews->getPortalSonnenklarTv());
		$this->assertEquals(1, $overlayNews->getPortalTest());
	}
}