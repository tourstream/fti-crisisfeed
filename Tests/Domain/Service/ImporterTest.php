<?php
namespace Fti\Crisisfeed\Tests\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Carsten Windler <carsten.windler@fti-ecom.de>, FTI eCom
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Testcase for the Importer Service
 *
 * @author Carsten Windler <carsten.windler@fti-ecom.de>
 * @package crisisfeed
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */
class ImporterTest extends \TYPO3\CMS\Core\Tests\UnitTestCase {

	protected $settings;

	/**
	 * @var \Fti\Crisisfeed\Domain\Service\Importer
	 */
	protected $fixture;

	public function setUp() {
		$this->settings = array(
			'settings.' => array(
				'fileUploadPath' => '/path/to',
				'client.' => array(
					'server' 		=> 'feedserver',
					'portalKey'  	=> 'myPortalKey',
				),
				'server.' => array(
					'feed.' => array(
						'locations.' => array(
							'feedserver' => 'www.feedserver.com',
						),
						'protocol' => 'http://',
						'parameters.' => array(
							'getActive' => 'type=76543&tx_crisisfeed_newsserver[format]=xml&tx_crisisfeed_newsserver[portalkey]=###PORTALKEY###',
						),
					),
				),
			),
		);

		$this->fixture = new \Fti\Crisisfeed\Domain\Service\Importer();

		$this->fixture->setSettings($this->settings);
	}

	/**
	 * @test
	 * @covers \Fti\Crisisfeed\Domain\Service\Importer::getUploadFolder
	 */
	public function canGetUploadFolder() {
		$this->assertEquals('/path/to', $this->fixture->getUploadFolder());
	}

	/**
	 * @test
	 * @covers \Fti\Crisisfeed\Domain\Service\Importer::setFeedUrlFromConfiguration
	 * @covers \Fti\Crisisfeed\Domain\Service\Importer::getCurrentNewsItems
	 * @covers \Fti\Crisisfeed\Domain\Service\Importer::readFeedIntoArray
	 */
	public function canGetCurrentNewsItemFromFeed() {
		$dataParserMock = $this->getMock('\\Fti\\Crisisfeed\\Domain\\Service\\DataParser');

		$xml = simplexml_load_file(__DIR__ . '/Assets/sampleRssFeed.xml');

		$dataParserMock->expects($this->any())
			->method('fetchUrlAsSimpleXml')
			->will($this->returnValue($xml));

		$this->fixture->injectDataParser($dataParserMock);

		$newsItems = $this->fixture->getCurrentNewsItems();
		$newsItem = array_shift($newsItems);
		$this->assertInstanceOf('\\Fti\\Crisisfeed\\Domain\\Model\\CurrentNews', $newsItem);
		$this->assertEquals('itemtitletest', $newsItem->getTitle());
		$this->assertEquals('itemsubheadertest', $newsItem->getSubheader());
		$this->assertEquals('itemcontenttest', $newsItem->getBodytext());
		$this->assertEquals('itemauthortest', $newsItem->getAuthor());
		$this->assertEquals('/path/to/image', $newsItem->getImage());
		$this->assertEquals('1417090680', $newsItem->getDatetime());   // Thu, 27 Nov 2014 13:18:00 CET
		$this->assertEquals('itemguidtest', $newsItem->getGuid());
	}

	/**
	 * @test
	 * @covers \Fti\Crisisfeed\Domain\Service\Importer::setFeedUrlFromConfiguration
	 * @covers \Fti\Crisisfeed\Domain\Service\Importer::getOverlayNewsItems
	 * @covers \Fti\Crisisfeed\Domain\Service\Importer::readOverlayFeedIntoArray
	 */
	public function canGetCurrentOverlayNewsItemFromFeed() {
		$dataParserMock = $this->getMock('\\Fti\\Crisisfeed\\Domain\\Service\\DataParser');

		$xml = simplexml_load_file(__DIR__ . '/Assets/sampleRssFeed.xml');

		$dataParserMock->expects($this->any())
				->method('fetchUrlAsSimpleXml')
				->will($this->returnValue($xml));

		$this->fixture->injectDataParser($dataParserMock);

		$overlayNewsItems = $this->fixture->getOverlayNewsItems();
		$overlayNewsItem = array_shift($overlayNewsItems);
		$this->assertInstanceOf('\\Fti\\Crisisfeed\\Domain\\Model\\CurrentOverlayNews', $overlayNewsItem);
		$this->assertEquals('overlaytitletest', $overlayNewsItem->getTitle());
		$this->assertEquals(1, $overlayNewsItem->getLive());
		$this->assertEquals('overlayheadertest', $overlayNewsItem->getHeader());
		$this->assertEquals('overlaysubheadertest', $overlayNewsItem->getSubheader());
		$this->assertEquals('overlaybodytext', $overlayNewsItem->getBodytext());
	}
}