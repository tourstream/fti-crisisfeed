// fetch active overlay news from a static file on the same server and show it in a "Dark Layer"
(function(ftiOverlayNews, $, undefined ) {

    var staticFilePath = "/fileadmin/crisisfeed/";
    var staticFileBaseName = "overlay.html";
    // stores whether user already saw an active overlay news
    var overlayCookieName = "overlay_crisis_news_hidden";
    // omniture reads this cookie; omniture overlays are not shown when this cookie is set
    var omnitureCookieName = "overlay_crisis_news";
    var showOverlayCallback = null;
    var hideOverlayCallback = null;

    function showOverlayNews(){
        $(".closeoverlay img").click(function() {
            hideOverlayNews();
        });
        if (typeof showOverlayCallback === "function") {
            showOverlayCallback();
        }
        $("#darkscreen").fadeIn();
        $("#overlay").fadeIn();
    }

    function hideOverlayNews(){
        $("#darkscreen").fadeOut();
        $("#overlay").fadeOut();
        if (typeof hideOverlayCallback === "function") {
            hideOverlayCallback();
        }
    }

    function fetchOverlayNews(){
        var fiveMinuteTimestamp = new Date(Math.ceil(new Date().getTime()/ 300000) * 300000).getTime();
        var staticFileLocation = staticFilePath + staticFileBaseName;

        $.ajax({
                // add timestamp to url as query parameter to avoid browser caching
                url: staticFileLocation + '?' + fiveMinuteTimestamp
            })
            .done(function( data ) {
                if (isOverlayMarkupValid(data)) {
                    $("#overlayblock_wrapper").html( data );
                    showOverlayNews();
                    // once the overlay is shown, don't show it again when opening a page in a new window/tab
                    document.cookie = overlayCookieName+"=1; path=/";
                    // this cookie is set only for handling in omniture
                    document.cookie = omnitureCookieName+"=1; path=/";
                }
            })
            .fail(function (){
                console.log('To get rid of the 404 error for ' + staticFileLocation + ', please run the "Crisis News Feed Importer (crisisfeed)" scheduler task');
            });
    }

    // returns true if the static file contains the elements we need
    function isOverlayMarkupValid(data){
        if (
            data.indexOf('id="overlayblock"') > -1 &&
            data.indexOf('id="overlay"') > -1 &&
            data.indexOf('id="darkscreen"') > -1 &&
            data.indexOf('class="closeoverlay"') > -1
        ) {
            return true;
        }
        return false;
    }
    //returns a truthly value if the cookie exist ~ 
    function cookieThere(cookieName) {
        var cookies = document.cookie,
            cookieFound = ~cookies.indexOf(cookieName);

        return cookieFound;
    }

    ftiOverlayNews.run = function (showCallback, hideCallback){
        // delete cookie used for omniture by default; gets re-set if there is an active overlay news
        document.cookie = omnitureCookieName + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        // register callback methods
        showOverlayCallback = showCallback;
        hideOverlayCallback = hideCallback;

        // preview mode is for the server component, only.
        // when active, overlay news need to already be available on the page via the DB
        if ($('#overlayblock.is-preview').length > 0){
            showOverlayNews();
            return;
        }

        // don't do anything when no overlay wrapper element is available
          if (!$('#overlayblock_wrapper').length > 0) {
              return;
          }

        // if the overlay cookie is still valid, the Overlay is not visible
        if (!cookieThere(overlayCookieName)) {
            fetchOverlayNews();
        }
    }

}( window.ftiOverlayNews = window.ftiOverlayNews || {}, jQuery ));